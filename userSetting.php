<?php

    session_start();
    include 'config/connection.php';
     date_default_timezone_set('Africa/Lagos');
     if (!isset($_SESSION['name'])){
header("Location:signIn.php");
exit();
}

    if (!isset($_SESSION['id']) || !isset($_POST['address']) || !isset($_POST['gender'])){
        if (!isset($_SESSION['id'])){
            try{
                $query = "SELECT ID FROM booust.users WHERE email=:email";
                $stmt = $connection->prepare($query);
                
                //Now to bind the parameters (variable names) with the names of the columns on DB
                $stmt->bindParam(':email', $_SESSION['theEmail']);
                $stmt->execute();
                
                $num = $stmt->rowCount();
                
                if($num > 0){
                    $row = $stmt->fetch();
                    $_SESSION['id'] = $row['0'];
                    header("Location: userSetting.php");
                    exit();
                            
                }
                
            }
            catch (PDOException $e){
                die('Error:'.$e->getMessage()); 
     
            }
        }
    }else{
        try{
            $query = "SELECT * FROM booust.profiles WHERE users_id=:id";
            
            $stmt = $connection->prepare($query);
            
            //binding time again... i like this binding, Low-key
            $stmt->bindParam(':id',$_SESSION['id']);
            $stmt->execute();
            
            $num = $stmt->rowCount();
            
            if ($num == 0 ){
                $query = "INSERT INTO booust.profiles SET users_id=:id, Home_Address=:address, Gender=:gender, created_at=:created_at";
                
                $stmt = $connection->prepare($query);
                
                
                $id = $_SESSION['id'];
                $address = $_POST['address'];
                $gender = $_POST['gender'];
                $createDate = date('Y-m-d H:i:s');
                
                $stmt->bindParam(':id', $id);
                $stmt->bindParam(':address', $address);
                $stmt->bindParam(':gender', $gender);
                $stmt->bindParam(':created_at', $createDate);
                
                if ($stmt->execute()){
                    header("Location: index.php");
                    exit();
                }
                else{
                    echo "<div class = 'alert alert-danger'>Error: </div>";
                    
                     
                }
            }
            else{
                
                $query = "UPDATE booust.profiles SET Home_Address=:address WHERE users_id=:id";
                
                $stmt = $connection->prepare($query);
                $stmt->bindParam(':id', $_SESSION['id']);
                $stmt->bindParam(':address',  $_POST['address']);
                
                
                if($stmt->execute()){
                    header("Location: index.php");
                    exit(); 
                }else{
                    echo "<div class='alert alert-danger'>Unable to update record.</div>";
                }
                     
                
                
            }
            
        }
        catch (PDOException $ee){
           die('Error:'.$e->getMessage());  
        }
    }


?>

<html lang="en">
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">  
      <title>util filmora</title>
      <link rel="stylesheet" type="text/css" href="css/index.css">
      <!-- <link rel="stylesheet" type="text/css" href="css/bootstrap.css"> -->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    </head>
    <body>
          <style> 
           .dropdown{
               float: left;
               
               
           }
           .dropdown-toggle{
               background-color: #333!important;
               border-color: #333!important;
               color:#eee!important;
               padding: 14px;
           }
</style>
        <nav class="navbar navbar-inverse" id= "navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>                        
                    </button>
                    <a class="navbar-brand" href="#">
                        <img id="brand-image" alt="Website Logo" src="images/utilityrental.png"/>
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav navbar-right" id= "unorderedList">
                    
                            <?php if(isset($_SESSION['name'])){
                                try{
                            $email = $_SESSION['theEmail'];
                            $result =("SELECT name FROM booust.users WHERE email =:email");
                            $stmt = $connection-> prepare($result);
                             $stmt->bindParam(':email', $email);
                            $stmt->execute();
                            $user = $stmt->fetch();
                            
                            echo  "<div class='dropdown'>
                                <button class='btn btn-default dropdown-toggle' type='button' id='dropdownMenu1' data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'><span class='glyphicon glyphicon-user'></span>&nbsp;
                                  $user[0]
                                  <span class='caret'></span>
                                </button>
                                <ul class='dropdown-menu' aria-labelledby='dropdownMenu1'>
                                  <li><a href='#'></a></li>
                                  <li><a href='dashboard.php'>Dashboard</a></li>
                                  <li><a href='userSetting.php'>Account Settings</a></li>
                                  <li role='separator' class='divider'></li>
                                  <li><a href='signIn.php'>Sign Out</a></li>
                                </ul>
                              </div>";
                            /*echo "<div class='dropdown'>
                                <button class='dropbtn' onclick='menuDrop()'>$user[0]
                                  <i class='fa fa-caret-down'></i>
                                </button>
                                <div class='dropdown-content' id='myDropdown'>
                                  <a href='#'>Link 1</a>
                                  <a href='#'>Link 2</a>
                                  <a href='#'>Link 3</a>
                                </div>
                              </div> ";*/
                            
                            //echo "<li><a href='signIn.php'><span class='glyphicon glyphicon-user'></span>&nbsp;</a></li>";
                                }
                            catch (PDOException $e){
                                    echo '';
                            }
                            } else{
                            echo "<li><a href='signIn.php'><span class='glyphicon glyphicon-user'></span> Sign In</a></li>";
                            }
                            ?>
                </ul>
                </div>
            </div>
        </nav>
        <div class="Container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <!--<li class="breadcrumb-item"><a href="index.php">Home</a></li>-->
                <li class="breadcrumb-item active" aria-current="Conta">Account settings</li>
                </ol>
            </nav> 
        </div>
        
        <div class="Container">
            <h3 style="padding:0!important;"><strong>General Account Settings</strong></h3>
           
            <br>
    
                    <label for="email" style="color: #333">Email Address</label>
                     <?php if(isset($_SESSION['name'])){
                                try{
                            $email = $_SESSION['theEmail'];
                            $result =("SELECT email FROM booust.users WHERE email =:email");
                            $stmt = $connection-> prepare($result);
                             $stmt->bindParam(':email', $email);
                            $stmt->execute();
                            $user = $stmt->fetch();
                            
                            echo "<input type='email' id = 'emm1'class='form-control' name='email' size='50' placeholder=$user[0]  readonly>";
                                }
                            catch (PDOException $ele){
                                    echo '';
                         }
                     }   else{
                            echo ''; 
                            }
                    ?>
                    <label for="name"style="margin-top: 20px; color: #333">Username</label>
                    <?php if(isset($_SESSION['name'])){
                                try{
                            $email = $_SESSION['theEmail'];
                            $result =("SELECT name FROM booust.users WHERE email =:email");
                            $stmt = $connection-> prepare($result);
                             $stmt->bindParam(':email', $email);
                            $stmt->execute();
                            $user = $stmt->fetch();
                            
                            echo "<input type='text' id='uName1' class='form-control' name='name' placeholder=$user[0] readonly>";
                                }
                            catch (PDOException $ele){
                                    echo '';
                         }
                     }   else{
                            echo '';
                            }
                    ?>
                    
                
                    <form  action="userSetting.php" method="POST"> 
                    <label for="gender"style=" margin-top: 20px; color: #333">Gender</label><br>
                    <input type="radio" name="gender" value="male" checked> Male
                    <input type="radio" name="gender" value="female"> Female &nbsp;<span style="font-size:10px; color: red">* Please note, this field can only be set once!</span><br>
                    
                    <label for="address" style=" margin-top: 20px; color: #333">Home Address</label>
                        <textarea id="subject" name="address"
                        placeholder="No 1 Agbalumo street, lagos..."
                        style="height:170px" required></textarea>
                    <button id="submitter" type="submit" class="galleria" style="margin-bottom: 20px!important;">Update</button> 
                    <p>Please read the <a href="termsOfUse.html" target="_blank" style="color:dodgerblue">Terms & Privacy</a> documentation for changes made to your account.</p>
                </form>
                     
                </div>
              
            
        
        
        <footer class="container-fluid text-center">
            <p>&copy 2018. Utility Production</p>      
        </footer>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> 
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </body>
</html>
