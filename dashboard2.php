<?php
session_start();
require 'config/connection.php';
if (!isset($_SESSION['name'])){
header("Location: signIn.php");

exit();}
/* 
$query = "SELECT * FROM booust.videos";
$stmt = $connection->prepare($query);
$stmt->execute();

*/
?>
<?php
if (!isset($_SESSION['id'])){
            try{
                $query = "SELECT ID FROM booust.users WHERE email=:email";
                $stmt = $connection->prepare($query);
                
                //Now to bind the parameters (variable names) with the names of the columns on DB
                $stmt->bindParam(':email', $_SESSION['theEmail']);
                $stmt->execute();
                
                $num = $stmt->rowCount();
                
                if($num > 0){
                    $row = $stmt->fetch();
                    $_SESSION['id'] = $row['0'];
                    
                    exit();
                            
                }
                
            }
            catch (PDOException $e){
                die('Error:'.$e->getMessage()); 
     
            }
        }
        ?>

<!DOCTYPE html>
<html lang="en"> 
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="css/index.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
</head>
<body>
<style>
    /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
    .row.content {height: 1500px}
    .row{
        margin-right: 0!important;
    }
    /* Set gray background color and 100% height */
    .sidenav {
      background-color: #f1f1f1;
      height: 100%;
    }
    .nav>li>a{
        margin-left: 10px;
        margin-right: 10px;
    }  
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height: auto;} 
    }
    .dropbtn {
    background-color:#f1f1f1;
    color: #337ab7;
    padding: 16px;
    font-size: 16px;
    border: none;
    border-radius: 4px;
    cursor: pointer;
    
}

.dropdown {
    position: relative;
    display: inline-block;
    margin-left: 10px;
   
    
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

.dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

.dropdown-content a:hover {background-color: #f1f1f1}

.dropdown:hover .dropdown-content {
    display: block;
}

.dropdown:hover .dropbtn {
    background-color: #f1f1f1;
    color: #337ab7;
    
}

.dropdown{
               float: left;
               
               
           }
           .dropdown-toggle{
               background-color: #333!important;
               border-color: #333!important;
               color:#eee!important;
               padding: 14px;
           }
           .well{
               margin-top: 20px;
               margin-right: 10px;
           }
  </style>
  <nav class="navbar navbar-inverse" id= "navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>                        
                    </button>
                    <a class="navbar-brand" href="#">
                        <img id="brand-image" alt="Website Logo" src="images/utilityrental.png"/>
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav navbar-right" id= "unorderedList">
                    
                            <?php if(isset($_SESSION['name'])){
                                try{
                            $email = $_SESSION['theEmail'];
                            $result =("SELECT name FROM booust.users WHERE email =:email");
                            $stmt = $connection-> prepare($result);
                             $stmt->bindParam(':email', $email);
                            $stmt->execute();
                            $user = $stmt->fetch();
                            
                            echo  "<div class='dropdown'>
                                <button class='btn btn-default dropdown-toggle' type='button' id='dropdownMenu1' data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'><span class='glyphicon glyphicon-user'></span>&nbsp;
                                  $user[0]
                                  <span class='caret'></span>
                                </button>
                                <ul class='dropdown-menu' aria-labelledby='dropdownMenu1'>
                                  <li><a href='#'></a></li>
                                  <li><a href='dashboard.php'>Dashboard</a></li>
                                  <li><a href='userSetting.php'>Account Settings</a></li>
                                  <li role='separator' class='divider'></li>
                                  <li><a href='signIn.php'>Sign Out</a></li>
                                </ul>
                              </div>";
                            /*echo "<div class='dropdown'>
                                <button class='dropbtn' onclick='menuDrop()'>$user[0]
                                  <i class='fa fa-caret-down'></i>
                                </button>
                                <div class='dropdown-content' id='myDropdown'>
                                  <a href='#'>Link 1</a>
                                  <a href='#'>Link 2</a>
                                  <a href='#'>Link 3</a>
                                </div>
                              </div> ";*/
                            
                            //echo "<li><a href='signIn.php'><span class='glyphicon glyphicon-user'></span>&nbsp;</a></li>";
                                }
                            catch (PDOException $e){
                                    echo '';
                            }
                            } else{
                            echo "<li><a href='signIn.php'><span class='glyphicon glyphicon-user'></span> Sign In</a></li>";
                            }
                            ?>
                </ul>
                </div>
            </div>
        </nav>
<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 sidenav">
        <h4 style="color: #337ab7; margin-left: 15px; margin-right: 15px; margin-top: 30px;">My Dashboard</h4>
      <ul class="nav nav-pills nav-stacked">
          <li class="active"><a href="index.php">Home</a></li>
        <div class="dropdown">
  <button class="dropbtn">Videos</button>
  <div class="dropdown-content">
      <a href="dashboard.php" class="pager1">All Videos</a>
      <a href="dashboard2.php" class="pager2">Upload Videos</a>
        
  </div>
</div>
      </ul><br>
      
    </div>
      
      <div class="col-sm-9">
        <div class="well">
            <h4>Upload A New Video</h4>
            
        </div>
          
          <form  action="dashboard2.php" method="post" enctype="multipart/form-data"> 
                    <label for="title" style=" margin-top: 20px; color: #333">Title</label><br>
                        <input type="text" id="title" class="form-control" name="title"
                               placeholder="Video title..." required>
                    <label for="genre" style=" margin-top: 20px; color: #333">Genre</label>
                        <input type="text" id="genre" class="form-control" name="genre"
                               placeholder="Video genre..." required> <br><br>
                    <input type="file" name="pic" id="pic">
                    <br><br>
                    <input type="submit" value="Upload" name="submit">   
                    </form>
<?php   
                            
            
 if($_POST){          
          $target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["pic"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
// Check if image file is a actual image or fake image
if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["pic"]["tmp_name"]);
    if($check !== false) {
        //echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
    //echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["pic"]["size"] > 500000) {
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Upload doesn't meet all requirement, but will try to execute anyway";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["pic"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["pic"]["name"]). " is being uploaded.";
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}
    try {
        $query = "INSERT INTO booust.videos set users_id=:id, title=:title, genre=:genre, pic_url=:pic_url, created_at=:created_at";
        $stmt = $connection->prepare($query);
        $title = $_POST['title'];
        $genre = $_POST['genre'];
        $pic_url = basename($_FILES["pic"]["name"]);
        $id = $_SESSION['id'];
        $created_at = date('Y-m-d H:i:s');
        
        $stmt->bindParam(':id', $id);
        $stmt->bindParam(':title', $title);
        $stmt->bindParam(':genre', $genre);
        $stmt->bindParam(':pic_url', $pic_url);
        $stmt->bindParam(':created_at', $created_at);
        
        if ($stmt->execute()){
            echo "<div class = 'alert alert-success'>Movie Uploaded</div>";
        
            exit();
        }else {
            echo "<div class = 'alert alert-danger'>Unable to upload video</div>";
        }
        
    }           
 catch (PDOException $e){}
    die('ERROR '.$e->getMessage());
 
 }
?>

  </div> </div>
</div>
 
<footer class="container-fluid">
  <p>Footer Text</p>
</footer>
   
</body>
</html>
