
<?php

//this line of code starts a new session on page load
session_start();
include 'config/connection.php';
if (!isset($_SESSION['name'])){
header("Location: signIn.php");

exit();}

?><html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">  
  <title>util filmora</title>
  <link rel="stylesheet" type="text/css" href="css/index.css">
  
  <!-- <link rel="stylesheet" type="text/css" href="css/bootstrap.css"> -->


  <!-- I'm using Max CDN because Ifeanyi said we could use what we were most comfortable with -->
  
  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  

</head>
<body>
      <style> 
           .dropdown{
               float: left;
               
               
           }
           .dropdown-toggle{
               background-color: #333!important;
               border-color: #333!important;
               color:#eee!important;
               padding: 14px;
           }
</style>
    <nav class="navbar navbar-inverse" id= "navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>                        
                </button>
                <a class="navbar-brand" href="#">
                    <img id="brand-image" alt="Website Logo" src="images/utilityrental.png"/>
                </a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right" id= "unorderedList">
                <li><a href="index.php">Home</a></li>
                <li  class="active"><a href="Deals.php">Deals</a></li>
                <li><a href="contact.php">Contact</a></li>
                                          <?php if(isset($_SESSION['name'])){
                                try{
                            $email = $_SESSION['theEmail'];
                            $result =("SELECT name FROM booust.users WHERE email =:email");
                            $stmt = $connection-> prepare($result);
                             $stmt->bindParam(':email', $email);
                            $stmt->execute();
                            $user = $stmt->fetch();
                            
                            echo  "<div class='dropdown'>
                                <button class='btn btn-default dropdown-toggle' type='button' id='dropdownMenu1' data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'><span class='glyphicon glyphicon-user'></span>&nbsp;
                                  $user[0]
                                  <span class='caret'></span>
                                </button>
                                <ul class='dropdown-menu' aria-labelledby='dropdownMenu1'>
                                  <li><a href='#'></a></li>
                                  <li><a href='dashboard.php'>Dashboard</a></li>
                                  <li><a href='userSetting.php'>Account Settings</a></li>
                                  <li role='separator' class='divider'></li>
                                  <li><a href='signIn.php'>Sign Out</a></li>
                                </ul>
                              </div>";
                            /*echo "<div class='dropdown'>
                                <button class='dropbtn' onclick='menuDrop()'>$user[0]
                                  <i class='fa fa-caret-down'></i>
                                </button>
                                <div class='dropdown-content' id='myDropdown'>
                                  <a href='#'>Link 1</a>
                                  <a href='#'>Link 2</a>
                                  <a href='#'>Link 3</a>
                                </div>
                              </div> ";*/
                            
                            //echo "<li><a href='signIn.php'><span class='glyphicon glyphicon-user'></span>&nbsp;</a></li>";
                                }
                            catch (PDOException $e){
                                
                            }
                            } else{
                            echo "<li><a href='signIn.php'><span class='glyphicon glyphicon-user'></span> Sign In</a></li>";
                            }
                            ?>
                            
  
            </ul>
            
            </div>
        </div>
    </nav>

    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="index.php">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page">Deals</li>
            </ol>
        </nav>
        <div class="container text-center">    
            <h2>Available Rentals Today</h2><br>
        </div>
        <div class="row">
            <div class="movie-card">
                <div class="card">
                    <img src="images/twilight.jpg" alt="Avatar" style="width:100%">
                    <div class="moviePrize">
                            <h4><b>Twilight 2</b></h4> 
                        <p><strong style="text-decoration: line-through;">N</strong><strong>50.00</strong></p>
                    </div>
                </div>
            </div>
            <div class="movie-card">
                <div class="card">
                    <img src="images/insurgent.jpg" alt="Avatar" style="width:100%">
                    <div class="moviePrize">
                        <h4><b>Insurgent</b></h4>   
                        <p><strong style="text-decoration: line-through;">N</strong><strong>50.00</strong></p>
                    </div>
                </div>
            </div>
            <div class="movie-card">
                <div class="card">
                    <img src="images/inception.jpg" alt="Avatar" style="width:100%">
                    <div class="moviePrize">
                        <h4><b>Inception</b></h4>
                        <p><strong style="text-decoration: line-through;">N</strong><strong>50.00</strong></p>
                    </div>
                </div>
            </div>
            <div class="movie-card">
                <div class="card">
                    <img src="images/incredibleHulk.jpg" alt="Avatar" style="width:100%">
                    <div class="moviePrize">
                        <h4><b>Incredible Hulk</b></h4> 
                        <p><strong style="text-decoration: line-through;">N</strong><strong>50.00</strong></p>
                    </div>
                </div>  
            </div>
        
            <div class="movie-card">
                <div class="card">
                    <img src="images/justiceLeague.jpg" alt="Avatar" style="width:100%">
                    <div class="moviePrize">
                        <h4><b>Justice League</b></h4> 
                        <p><strong style="text-decoration: line-through;">N</strong><strong>79.99</strong></p> 
                    </div>
                </div>
            </div>
            <div class="movie-card"> 
                <div id=" cardo" class="card">
                    <img src="images/blended.jpg" alt="Avatar" style="width:100%">
                    <div class="moviePrize">
                        <h4><b>Blended</b></h4> 
                        <p><strong style="text-decoration: line-through;">N</strong><strong>29.99</strong></p>
                    </div>
                </div>  
            </div>
            <div class="movie-card">
                <div class="card">
                    <img src="images/bigSick.JPG" alt="Avatar" style="width:100%">
                    <div class="moviePrize">
                        <h4><b>The Big Sick</b></h4> 
                        <p><strong style="text-decoration: line-through;">N</strong><strong>59.99</strong></p>
                    </div>
                </div>  
            </div>
            <div class="movie-card">
                <div class="card">
                    <img src="images/babyDriver.jpg" alt="Avatar" style="width:100%">
                    <div class="moviePrize">
                        <h4><b>Baby Driver</b></h4> 
                        <p><strong style="text-decoration: line-through;">N</strong><strong>50.00</strong></p>
                    </div>
                </div>  
            </div>
            <div class="movie-card">
                <div class="card">
                    <img src="images/hitman.jpg" alt="Avatar" style="width:100%">
                    <div class="moviePrize">
                        <h4><b>The Hitman's Bodyguard</b></h4> 
                        <p><strong style="text-decoration: line-through;">N</strong><strong>59.50</strong></p>
                    </div>
                </div>
            </div>
            <div class="movie-card"> 
                <div class="card">
                    <img src="images/deadpool2.jpg" alt="Avatar" style="width:100%">
                    <div class="moviePrize">
                        <h4><b>Deadpool 2</b></h4> 
                        <p><strong style="text-decoration: line-through;">N</strong><strong>79.99</strong></p>
                    </div>
                </div>  
            </div>
            <div class="movie-card">
                <div class="card">
                    <img src="images/weddingParty.jpg" alt="Avatar" style="width:100%">
                    <div class="moviePrize">
                        <h4><b>Wedding Party 2</b></h4> 
                        <p><strong style="text-decoration: line-through;">N</strong><strong>60.50</strong></p>
                    </div>
                </div>  
            </div>
            <div class="movie-card">
                <div class="card">
                    <img src="images/beautyAndBeast.jpg" alt="Avatar" style="width:100%">
                    <div class="moviePrize">
                        <h4><b>Beauty And The Beast</b></h4> 
                        <p><strong style="text-decoration: line-through;">N</strong><strong>50.00</strong></p>
                    </div>
                </div>  
            </div>
               
          
            <div class="movie-card">
                <div class="card">
                    <img src="images/iboy.jpg" alt="Avatar" style="width:100%">
                    <div class="moviePrize">
                        <h4><b>IBoy</b></h4> 
                        <p><strong style="text-decoration: line-through;">N</strong><strong>45.55</strong></p>
                    </div>
                </div>
            </div>
            <div class="movie-card"> 
                <div class="card">
                    <img src="images/jummy.jpg" alt="Avatar" style="width:100%">
                    <div class="moviePrize">
                        <h4><b>Jumanji</b></h4> 
                        <p><strong style="text-decoration: line-through;">N</strong><strong>65.40</strong></p>
                    </div>
                </div>  
            </div>
            <div class="movie-card">
                <div class="card">
                    <img src="images/security.jpg" alt="Avatar" style="width:100%">
                    <div class="moviePrize">
                        <h4><b>Security</b></h4> 
                        <p><strong style="text-decoration: line-through;">N</strong><strong>30.00</strong></p>
                    </div>
                </div>  
            </div>
            <div class="movie-card">
                <div class="card">
                    <img src="images/commuter.jpg" alt="Avatar" style="width:100%">
                    <div class="moviePrize">
                        <h4><b>The Commuter</b></h4> 
                        <p><strong style="text-decoration: line-through;">N</strong><strong>50.00</strong></p>
                    </div>
                </div>  
            </div>
                    </div>
                </div>                
 

    <footer class="container-fluid text-center">
        <p>&copy 2018. Utility Production</p>  
        
      </footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> 
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <!-- <script src="js/jquery.js"></script>
  <script src="js/bootstrap.js"></script> -->
</body>
</html>