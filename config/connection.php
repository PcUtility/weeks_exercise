<?php
require_once 'dbconfig.php';
try {
    $connection = new PDO("mysql:host={$host};dbname={$db_name}", $user, $pass); 
        
    //$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
//catch any errors
catch (PDOException $exception){
    echo "Failed to connect to database for some reason(s): ". $exception->getMessage(); 
}
        
        ?>