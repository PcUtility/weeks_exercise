 <?php

/*

 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of contactUs
 *
 * @author Okeugo
 */


//Load Composer's autoloader
require 'vendor/autoload.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
    
 class contactUs {
         

    public static function process($param){
        date_default_timezone_set('Africa/Lagos');
      
        
        /*
         |_________________________________________________________________________________
         | This code creates a csv file called contact.csv and the mode set to "a" append
         *________________________________________________________________________________ 
         */
        $file = fopen('contact.csv', 'a');
        
        //fputcsv($file, array('FirstName','LastName', 'Email', 'Country', 'Message', 'Date'));
        
        //well since i'm always confusing the american date format,  
        //date(DATE_RFC2822) would append something like this "Wed, 25 Sep 2013 15:28:57 -0700"
        fputcsv($file, array($param['firstname'],$param['lastname'],$param['email'],$param['country'],$param['message'],  date(DATE_RFC2822)));
        
        //close file stream
        fclose($file);
        
        //This prepares email message for the user
        $message = "Hi ".$param['firstname'].",\n\nWe appreciate your suggestion and we would look into it ASAP. \n\nWarm Regards,\nUtility Films.";
        //$headers = "Reply-To: Movie Admin <pc.okeugo@ulelulinks.com>\r\n";
        //this is a send mail function. it is a default php function
        //$theHead = "Reply-To: Movie Admin <admin@utilmovies.com>\r\n";
        //$theHead .= "Return-Path: Movie Admin <admin@utilmovies.com>\r\n";
        $theHead = "From: Movie Admin <pc.okeugo@ulelulinks.com>";
        
        //mail($param['email'], "Utility Movies", $message, $theHead); 
        /*ini_set('SMTP', "smtp.gmail.com");
        ini_set('smtp_port', "465");
        ini_set('sendmail_from', "princewillutility@gmail");*/
        
        
         /*
         |_________________________________________________________________________________
         | The following line of code is just to fulfill requirement specification 
          * thus: "The information should be retrieved from the file in which it was stored."
         *________________________________________________________________________________ 
         */
        

$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
try {
    //Server settings
    //$mail->SMTPDebug = 2;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'ssl://smtp.gmail.com';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = true;                               // Enable SMTP authentication
    $mail->Username = 'princewillutility@gmail.com';                 // SMTP username
    $mail->Password = 'classified';                           // SMTP password
    $mail->SMTPSecure = 'TLS';                            // Enable TLS encryption, `ssl` also accepted
    $mail->Port = 465;                                    // TCP port to connect to

    //sender
    $mail->setFrom('admin@utilmovie.com', 'Mr Utility');
    
    //Recipient
    $mail->addAddress($param['email'], $param['email']);     // Add a recipient
    
    $mail->addReplyTo('pc.okeugo@ulelulinks.com', 'Information');
    //$mail->addCC('me@olubisiakintunde.com');
    //$mail->addBCC('capt_princewill@yahoo.com');

    //Attachments
    //$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    //Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Utility Movies Contact';
    $mail->Body    = $message;
    $mail->AltBody = strip_tags($message);

    $mail->send();
    
} catch (Exception $e) {
    echo '';
}


        $csv = array_map('str_getcsv', file('contact.csv'));
        echo $csv[sizeof($csv)-1] [0].'<br>';
        echo $csv[sizeof($csv)-1] [1].'<br>';
        echo $csv[sizeof($csv)-1] [2].'<br>';
        echo $csv[sizeof($csv)-1] [3].'<br>';
        echo $csv[sizeof($csv)-1] [4].'<br>';

        
    }    
    
}