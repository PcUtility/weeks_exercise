<?php

//this line of code starts a new session on page load
session_start();

include 'config/connection.php';
if (!isset($_SESSION['name'])){
header("Location: signIn.php");

exit();
}

?>

<html lang="en">
    <head>
             <meta charset="UTF-8">
             <meta name="viewport" content="width=device-width, initial-scale=1.0">
             <meta http-equiv="X-UA-Compatible" content="ie=edge">
             <title>util filmora</title>
             <link rel="stylesheet" type="text/css" href="css/index.css">

             <!-- <link rel="stylesheet" type="text/css" href="css/bootstrap.css"> -->
             <!-- I'm using Max CDN because Ifeanyi said we could use what we were most
             comfortable with -->
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
   </head>
    <body>
      <style> 
           .dropdown{
               float: left;
               
               
           }
           .dropdown-toggle{
               background-color: #333!important;
               border-color: #333!important;
               color:#eee!important;
               padding: 14px;
           }
</style>
        <header class="header">
            <div class="header-overlay">
                <nav class="navbar navbar-inverse" id="navigation">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse"
                                data-target="#myNavbar">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="#">
                                <img id="brand-image" alt="Website Logo"
                                src="images/utilityrental.png"/>
                            </a>
                        </div>
                        <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav navbar-right" id="unorderedList">
                            <li class="active"><a href="index.php">Home</a></li>
                            <li><a href="Deals.php">Deals</a></li>
                            <li><a href="contact.php">Contact</a></li>
                            
                            <?php if(isset($_SESSION['name'])){
                                try{
                            $email = $_SESSION['theEmail'];
                            $result =("SELECT name FROM booust.users WHERE email =:email");
                            $stmt = $connection-> prepare($result);
                             $stmt->bindParam(':email', $email);
                            $stmt->execute();
                            $user = $stmt->fetch();
                            
                            echo  "<div class='dropdown'>
                                <button class='btn btn-default dropdown-toggle' type='button' id='dropdownMenu1' data-toggle='dropdown' aria-haspopup='true' aria-expanded='true'><span class='glyphicon glyphicon-user'></span>&nbsp;
                                  $user[0]
                                  <span class='caret'></span>
                                </button>
                                <ul class='dropdown-menu' aria-labelledby='dropdownMenu1'>
                                  <li><a href='#'></a></li>
                                  <li><a href='dashboard.php'>Dashboard</a></li>
                                  <li><a href='userSetting.php'>Settings</a></li>
                                  <li role='separator' class='divider'></li>
                                  <li><a href='signIn.php'>Sign Out</a></li>
                                </ul>
                              </div>";
                            /*echo "<div class='dropdown'>
                                <button class='dropbtn' onclick='menuDrop()'>$user[0]
                                  <i class='fa fa-caret-down'></i>
                                </button>
                                <div class='dropdown-content' id='myDropdown'>
                                  <a href='#'>Link 1</a>
                                  <a href='#'>Link 2</a>
                                  <a href='#'>Link 3</a>
                                </div>
                              </div> ";*/
                            
                            //echo "<li><a href='signIn.php'><span class='glyphicon glyphicon-user'></span>&nbsp;</a></li>";
                                
                            
                                }
                            catch (PDOException $e){
                                
                            }
                            } else{
                            echo "<li><a href='signIn.php'><span class='glyphicon glyphicon-user'></span> Sign In</a></li>";
                            }
                            ?>
                            
                        </ul>
                        </div>
                    </div>
                </nav>
                <div style="padding-left: 0; padding-right:0;" class= "container text-center header-col">
                    <h3 class="opener"> <strong>Welcome!</strong></h3>
                    <h3 class="opener2"> <strong>Check out our movies.</strong></h3>
                    <h3 class="opener3">Watch anywhere. Cancel Anytime</h3>
                    <br>
                   
                     <?php if(isset($_SESSION['name'])): ?>
                            
                            <?php else: ?>
                    <a href = "signUpStep3.php"><button class="galleria" id="freeReg">Free Sign Up</button></a>
                            <?php endif; ?>
                            
                    
                </div>
            </div>
        </header>
        
        <div class="row" id = "movieOptions"  style="margin: 0!important;">
            <div class="container text-center" style="color: #333">
                <div class="col-sm-4" >
                    <a>
                        <img id="cancels" alt="Website Logo" src="images/cancel.jpg">
                    </a>
                    <p><strong>No commitments<br>Cancel online anytime you want </strong> </p>
                </div>
                <div class="col-sm-4">
                    <a> 
                        <img id="devices" alt="Website Logo" src="images/devices.png">
                    </a>
                    <p><strong>Watch on any device<br>Anytime of the day</strong></p>
                </div>
                <div class="col-sm-4">
                    <a>
                        <img id="pockets" alt="Website Logo" src="images/pocket.png">
                    </a>
                    <p><strong>Pick a price<br>Based on your pocket</strong></p>
                </div>
            </div>
        </div>
        <div class="container-fluid" id="firstPager">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                </ol>
            
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <img src="images/got copy.PNG" alt="Image">
                    <div class="carousel-caption">
                    <h3>Makes More Than $1,000,000,000 Each Year</h3>
                    <p>Ranked 1st</p>
                    </div>      
                </div>
            
                <div class="item">
                    <img src="images/avengers-infin copy.PNG" alt="Image">
                    <div class="carousel-caption">
                    <h3>Made $675,291,101</h3>
                    <p>Ranked 2nd</p>
                    </div>      
                </div>
                <div class="item">
                    <img src="images/greatest showman copy.PNG" alt="Image">
                    <div class="carousel-caption">
                        <h3>Made $230,000,000</h3>
                        <p>Ranked 3rd</p>
                    </div>      
                    </div>
                    <div class="item">
                        <img src="images/jumanji copy.PNG" alt="Image">
                        <div class="carousel-caption">
                        <h3>Made $229,293,290</h3>
                        <p>Ranked 4th</p>
                        </div>      
                    </div>
                </div>
            
            
                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
                </a>
            </div>
            <div class="container">
            <!-- <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page">Home</li>
                </ol>
            </nav> -->

            <div class="jumbotron" id="introtextHolder">
                <h1 id = "introtext1"style="font-size: 40px;">Utility Films</h1>
                <p id= "introtext"style="font-size:20px; color: #eee;">With over twenty years of experience in the
                film industry, we ensure that every home in Africa is positively
                influenced by the distribution and availability of
                UtilityCo lisenced movies, documentary and books. We aim to be the leading film distributors in all of the Sub
                Sahara and Afica as a whole, as part of utilityCo.'s
                ongoing project to Eliminate Western Illiteracy In
                Africa (EWIIA) </p>
            </div>

            <div style="text-align:center;padding-left: 0;width: 100%; padding-right:0;" class="container text-center">
                <button class="galleria" onclick="showPage()">Learn More</button>
            </div>

            <div id="loader"></div>

            <div style="display:none;" id="myDiv" class="animate-bottom">

                <div class="container text-center">
                <h2> <strong>Meet Our Team </strong></h2><br><br><br>
                <div class="row">
                    <div class = "col-sm-4">
                    <a>
                        <img id="team2" alt="Website Logo" src="images/team2.png">
                    </a>
                    <br><br>
                    <h2>CFO</h2>
                    <h6>Heads finance department. She meets with shareholders of the company, 
                        reconciles accounts of the company monthly and invites the best external auditors 
                        to cross-evaluate the state of accounts of the business 
                    </h6><br><br>
                    </div>
                    <div class = "col-sm-4">
                        <a>
                            <img id="team1" alt="Website Logo" src="images/team.jpg">
                        </a>
                        <br><br>
                        <h2>COO</h2>
                        <h6>Daily workings of the business are his business.  He consolidates the company structure,
                            manages Human Resources, procurement and in-house business transactions with costumers. 
                            He is also the Head, Board Of Trustees
                        </h6><br><br>
                    </div>
                    <div class = "col-sm-4">
                        <a>
                        <img id="team3" alt="Website Logo" src="images/team3.jpg">
                        </a> <br><br>
                        <h2>CTO</h2>
                        <h6>Heads the Information Technology department of the company. He is also
                            a key stakeholder in the EWIIA campaign in Africa
                        </h6><br><br>
                    </div>
                </div>
                </div>
                </div>   
            </div>   

                <br><br><br><br><br><br><br><br><br>    

            <footer class="container-fluid text-center" id= "footing">
                <p>&copy 2018. Utility Production</p>
            </footer>
        </div>  
    
    <script>
    
        function showPage() {
        document.getElementById("loader").style.display = "none";
        document.getElementById("myDiv").style.display = "block";
        }
    </script>
 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
    <script>
/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function menuDrop() {
    document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(e) {
  if (!e.target.matches('.dropbtn')) {
    var myDropdown = document.getElementById("myDropdown");
      if (myDropdown.classList.contains('show')) {
        myDropdown.classList.remove('show');
      }
  }
}
</script>
    <script>
            $("#doneCheck").hide();
            $(document).ready(function(){ 
            
            if ($(window).width() > 355) {
                $("#freeReg").click(function(){
                var anni = $("#freeReg");
                anni.animate({

                bottom: '30px',
                opacity: '0.8',
                height: '100px',
                width: '300px'

                },"fast");
                anni.animate ({opacity:'0'},'fast');
                $("#doneCheck").show(2000);

                });

            }
            else {       
                $("#freeReg").click(function(){
                var anni = $("#freeReg");
                anni.animate({
            
                bottom: '30px',
                opacity: '0.8',
                height: '70px',
                width: '200px'

                },"fast");
                anni.animate ({opacity:'0'},'fast');
                });
            }
        
            });
        </script>
        
        <!-- <script src="js/jquery.js"></script>
        <script src="js/bootstrap.js"></script> -->
    </body>
</html>